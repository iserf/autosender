package com.blogspot.iserf.autosender;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.blogspot.iserf.autosender.model.Csv;
import com.blogspot.iserf.autosender.model.User;
import com.blogspot.iserf.autosender.util.ReceiverWhenSMSDelivered;
import com.blogspot.iserf.autosender.util.ReceiverWhenSMSSent;
import com.blogspot.iserf.autosender.util.SimpleFileDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends Activity implements View.OnClickListener {

    private EditText output;
    private Csv csv;
    private String csvFilePath;
    public String errorDetect;
    public int sendMessageCount;
    public int deliveredMessageCount;
    public int attempToSendMessage;
    public int totalUserInCsv;
    public User userInProcessing;
    public static String SERVICE_PHONE_NUMBER = "3344";
    private static String TAG = "SMS_ISERF";
    private static String DISPLAY_TAG = "SMS_ISERF_DISPLAY";
    public static MainActivity mainActivityForExternalCall;


    private boolean isRegisteredReceiverWhenSMSDelivered = false;
    private ReceiverWhenSMSDelivered receiverWhenSMSDelivered;
    private boolean isRegisteredReceiverWhenSMSSent = false;
    private ReceiverWhenSMSSent receiverWhenSMSSent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        output = (EditText) findViewById(R.id.textOut);

        receiverWhenSMSDelivered = new ReceiverWhenSMSDelivered(this);
        receiverWhenSMSSent = new ReceiverWhenSMSSent(this);

        Button openExelFile = (Button) findViewById(R.id.openExelFile);
        openExelFile.setOnClickListener(this);

        Button runSMS = (Button) findViewById(R.id.runSms);
        runSMS.setOnClickListener(this);

        mainActivityForExternalCall = this;
    }

    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.runSms:
                runSmsButtonAction();
                break;
            case R.id.openExelFile:
                openButtonAction();
                break;
        }
    }

    private void openButtonAction()
    {
        /////////////////////////////////////////////////////////////////////////////////////////////////
        //Create FileOpenDialog and register a callback
        /////////////////////////////////////////////////////////////////////////////////////////////////
        SimpleFileDialog FileOpenDialog = new SimpleFileDialog(MainActivity.this, "FileOpen:",
                new SimpleFileDialog.SimpleFileDialogListener() {
                    @Override
                    public void onChosenDir(String chosenDir) {
                        // The code in this function will be executed when the dialog OK button is pushed
                        File fileTemplate = new File(chosenDir);
                        if(!fileTemplate.isFile()){
                            printlnToUser("Choose csv file! You didn't choose file.");
                            return;
                        }

                        csvFilePath = chosenDir;
                        Toast.makeText(MainActivity.this, "Chosen file: " +
                                csvFilePath, Toast.LENGTH_LONG).show();
                        printlnToUser("Chosen csv file: " + csvFilePath);
                    }
                });

        //You can change the default filename using the public variable "Default_File_Name"
        FileOpenDialog.Default_File_Name = "";
        FileOpenDialog.chooseFile_or_Dir();
    }

    private void runSmsButtonAction(){
        if (csvFilePath == null) {
            printlnToUser("First choose csv file!");
        } else {

            printlnToUser("initSendingSystem ...");
            initSendingSystem();
            printlnToUser("sendSMSRun ...");
            sendSMSRun();
        }
    }

    private void initSendingSystem()
    {
        sendMessageCount =0;
        deliveredMessageCount = 0;
        attempToSendMessage = 0;
        userInProcessing= null;
        errorDetect = null;
        totalUserInCsv=0;

    }

    public void sendSMSRun()
    {
        if(errorDetect != null){
            printlnToUser(errorDetect);
            return;
        }

        if(userInProcessing != null){
            printlnToUser("Detect error: CSV Wait Response, cann't send!");
            return;
        }

        if(attempToSendMessage != sendMessageCount || sendMessageCount!= deliveredMessageCount){
            printlnToUser("Detect error: attempToSendMessage not equal with sendMessageCount!");
            return;
        }
        csv =  Csv.getInstance();
        csv.setCsvFileName(csvFilePath);
        List<User> userList = csv.getUserListFromCsvFile();
        totalUserInCsv =userList.size();
        if(totalUserInCsv < 1){
            printlnToUser("Detect error: csv file doesn't has any users!");
            return;
        }
        // for test
        //   System.out.println("sendSMSRun: " +java.util.Arrays.toString(userList.toArray()));
        for (User user : userList) {

            if(user.isChecked()==false) {
                String smsText = "KT#" + user.getTaxIdNumb();
                sendSMS(SERVICE_PHONE_NUMBER, smsText);
                userInProcessing = user;
                attempToSendMessage++;

                Log.d(TAG, "send SMS with text: " + smsText);
                return;
            }
        }

        printlnToUser("All users is checked! Task complite!");
        Toast.makeText(MainActivity.this, "Task Finish!",
                Toast.LENGTH_SHORT).show();
    }

    public void setStatusForUser(User changedUser) {
        csv =  Csv.getInstance();
        csv.setCsvFileName(csvFilePath);
        List<User> userList = csv.getUserListFromCsvFile();
        List<User> userListNew = new ArrayList<User>();
        for(int i=0; i<userList.size(); i++ ){
            User user = userList.get(i);
            if(user.getTaxIdNumb() == changedUser.getTaxIdNumb()){
                System.out.println("DETECT AND INSERT!");
                userListNew.add(changedUser);
            }else{
                userListNew.add(user);
            }
        }
        // for test
    //    System.out.println("setStatusForUser: " +java.util.Arrays.toString(userListNew.toArray()));
        csv.writeUserListInCsv(userListNew);
        userInProcessing=null;

    }


    private void sendSMS(String phoneNumber, String message)
    {
        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(MainActivity.this, 0,
                new Intent(SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(MainActivity.this, 0,
                new Intent(DELIVERED), 0);

        if(isRegisteredReceiverWhenSMSSent){
            unregisterReceiver(receiverWhenSMSSent);
        }
        registerReceiver(receiverWhenSMSSent, new IntentFilter(SENT));
        isRegisteredReceiverWhenSMSSent = true;


        if(isRegisteredReceiverWhenSMSDelivered){
            unregisterReceiver(receiverWhenSMSDelivered);
        }
        registerReceiver(receiverWhenSMSDelivered, new IntentFilter(DELIVERED));
        isRegisteredReceiverWhenSMSDelivered = true;

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
    }

    /**
     * print line to the output TextView
     *
     * @param str
     */
    public void printlnToUser(String str) {
        final String string = str;
        if (output.length() > 8000) {
            CharSequence fullOutput = output.getText();
            fullOutput = fullOutput.subSequence(5000, fullOutput.length());
            output.setText(fullOutput);
            output.setSelection(fullOutput.length());
        }
        output.append(string + "\n");
        Log.d(DISPLAY_TAG, string);
    }


    @Override
    protected void onStop()
    {
        if(isRegisteredReceiverWhenSMSSent){
            unregisterReceiver(receiverWhenSMSSent);
        }
        isRegisteredReceiverWhenSMSSent = false;


        if(isRegisteredReceiverWhenSMSDelivered){
            unregisterReceiver(receiverWhenSMSDelivered);
        }
        isRegisteredReceiverWhenSMSDelivered = false;
        super.onStop();
    }



}
