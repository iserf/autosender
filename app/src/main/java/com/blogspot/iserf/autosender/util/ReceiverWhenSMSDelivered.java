package com.blogspot.iserf.autosender.util;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.blogspot.iserf.autosender.MainActivity;


/**
 * Created by alex on 8/30/16.
 */
public class ReceiverWhenSMSDelivered extends BroadcastReceiver {

    private MainActivity mainActivity;
    private static String TAG = "SMS_ISERF_DELIVER";

    public ReceiverWhenSMSDelivered(MainActivity mainActivity){
        this.mainActivity = mainActivity;
    }


    @Override
    public void onReceive(Context arg0, Intent arg1) {
        switch (getResultCode())
        {
            case Activity.RESULT_OK:
                mainActivity.deliveredMessageCount++;
                Log.i(TAG,"SMS delivered");

                break;
            case Activity.RESULT_CANCELED:
                Log.i(TAG,"SMS not delivered");
                mainActivity.printlnToUser("Error: SMS not delivered");
                break;
        }
    }
}
