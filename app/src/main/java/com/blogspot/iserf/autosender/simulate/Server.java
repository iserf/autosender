package com.blogspot.iserf.autosender.simulate;

import com.blogspot.iserf.autosender.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alex on 8/31/16.
 */
public class Server {


    public static boolean getVerdict(String taxNumber) {

        User user1 = new User(new Long("2261600750"), false);
        User user2 = new User(new Long("2613900952"), true);
        User user3 = new User(new Long("2599900906"), true);
        User user4 = new User(new Long("2237800816"), false);
        User user5 = new User(new Long("3134416214"), false);
        User user6 = new User(new Long("3286004757"), false);
        User user7 = new User(new Long("2194916714"), true);
        User user8 = new User(new Long("3402513084"), true);
        User user9 = new User(new Long("3020105613"), true);
        User user10 = new User(new Long("3013711606"), true);
        User user11 = new User(new Long("2435800959"), false);
        User user12 = new User(new Long("3209006151"), true);
        User user13 = new User(new Long("1524517426"), false);
        User user14 = new User(new Long("3477301347"), true);
        List<User> userList = new ArrayList<User>();
        userList.add(user1);
        userList.add(user2);
        userList.add(user3);
        userList.add(user4);
        userList.add(user5);
        userList.add(user6);
        userList.add(user7);
        userList.add(user8);
        userList.add(user9);
        userList.add(user10);
        userList.add(user11);
        userList.add(user12);
        userList.add(user13);
        userList.add(user14);


        for (User user : userList) {
            if (("KT#" + user.getTaxIdNumb()).equals(taxNumber)) {
                if (user.isVerdict()) {
                    return true;
                } else {
                    return false;
                }
            }
        }


        throw new RuntimeException("Error user not found!");

    }


}
