package com.blogspot.iserf.autosender.model;

/**
 * Created by alex on 8/26/16.
 */
public class User {

    private String name;
    private long taxIdNumb;
    private boolean verdict;
    private boolean checked;
    private String infoMessage;
    private String phoneNumber;

    public User(){

    }

    public User(String name, long taxIdNumb, boolean verdict, boolean checked,
                String infoMessage, String phoneNumber){
        this.name = name;
        this.taxIdNumb = taxIdNumb;
        this.verdict= verdict;
        this.checked=checked;
        this.infoMessage=infoMessage;
        this.phoneNumber=phoneNumber;

    }
    public User(long taxIdNumb, boolean verdict){
        this.taxIdNumb = taxIdNumb;
        this.verdict= verdict;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getTaxIdNumb() {
        return taxIdNumb;
    }

    public void setTaxIdNumb(long taxIdNumb) {
        this.taxIdNumb = taxIdNumb;
    }

    public boolean isVerdict() {
        return verdict;
    }

    public void setVerdict(boolean verdict) {
        this.verdict = verdict;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getInfoMessage() {
        return infoMessage;
    }

    public void setInfoMessage(String infoMessage) {
        this.infoMessage = infoMessage;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {

        return "|| name: " + name + " taxIdNumb:" + taxIdNumb
                + " verdict:" + verdict +  " checked:" +checked + " phoneNumber:"+phoneNumber;
    }
}
