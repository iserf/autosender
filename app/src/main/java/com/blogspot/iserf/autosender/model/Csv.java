package com.blogspot.iserf.autosender.model;

import org.workcast.streams.CSVHelper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alex on 8/28/16.
 */
public class Csv {

    private List<User> userList;
    private String csvFileName;
    private final int NAME_COLUMN_INDEX = 0;
    private final int TAXID_COLUMN_INDEX = 1;
    private final int PHONENUMB_COLUMN_INDEX = 2;
    private final int VERDICT_COLUMN_INDEX = 3;

    private static Csv _instance = null;

    public static synchronized Csv getInstance() {
        if (_instance == null)
            _instance = new Csv();
        return _instance;
    }

    private Csv() {
    }

    public List<User> getUserListFromCsvFile() {

        userList = new ArrayList();
        File fileTemplate = new File(getCsvFileName());
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(fileTemplate);

            Reader fr = new InputStreamReader(fis, "UTF-8");

            List<String> values = CSVHelper.parseLine(fr);
            while (values != null) {

                User user = new User();
                String userName = values.get(NAME_COLUMN_INDEX);
                user.setName(userName);
                user.setTaxIdNumb(new Long(values.get(TAXID_COLUMN_INDEX)));
                user.setPhoneNumber(values.get(PHONENUMB_COLUMN_INDEX));


                if (values.size() < VERDICT_COLUMN_INDEX + 1) {
                    user.setChecked(false);
                } else {
                    String verdict = values.get(VERDICT_COLUMN_INDEX);
                    if (verdict.equals("+")) {
                        user.setChecked(true);
                        user.setVerdict(true);
                    } else if (verdict.equals("-")) {
                        user.setChecked(true);
                        user.setVerdict(false);
                    }

                }


                userList.add(user);

                values = CSVHelper.parseLine(fr);
            }

            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                fis.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        return userList;
    }

    public void writeUserListInCsv(List<User> userList) {

        File csvFile = new File(getCsvFileName());
        FileOutputStream fos = null;
        Writer fw = null;
        try {
            fos = new FileOutputStream(csvFile);

            fw = new OutputStreamWriter(fos, "UTF-8");

            for (User user : userList) {
                List<String> rowValues = new ArrayList<String>();
                rowValues.add(user.getName());
                rowValues.add(new Long(user.getTaxIdNumb()).toString());
                rowValues.add(user.getPhoneNumber());
                if (user.isChecked()) {
                    if (user.isVerdict()) {
                        rowValues.add("+");
                    } else {
                        rowValues.add("-");
                    }

                } else {
                    rowValues.add("");
                }
                CSVHelper.writeLine(fw, rowValues);
            }


            fw.flush();
            fw.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                fw.flush();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            try {
                fw.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }



    public String getCsvFileName() {
        return csvFileName;
    }

    public void setCsvFileName(String csvFileName) {
        this.csvFileName = csvFileName;
    }
}
