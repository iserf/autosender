package com.blogspot.iserf.autosender.util;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import com.blogspot.iserf.autosender.MainActivity;
import com.blogspot.iserf.autosender.simulate.Server;

/**
 * Created by alex on 8/30/16.
 */
public class ReceiverWhenSMSSent extends BroadcastReceiver {

    private MainActivity mainActivity;
    private static String TAG = "SMS_ISERF_SEND";

    public ReceiverWhenSMSSent(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }


    @Override
    public void onReceive(Context context, Intent intent) {
        switch (getResultCode()) {
            case Activity.RESULT_OK:

                mainActivity.sendMessageCount++;
                Log.i(TAG,"SMS RESULT_OK! totalUserInCsv: " + mainActivity.totalUserInCsv + "  sendMessageCount: " + mainActivity.sendMessageCount);
                mainActivity.printlnToUser("Send OK! Total users: " + mainActivity.totalUserInCsv + "  sent count: " + mainActivity.sendMessageCount);

                break;
            case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                Toast.makeText(mainActivity, "Generic failure",
                        Toast.LENGTH_SHORT).show();
                mainActivity.printlnToUser("Error while SMS sending: Generic failure");
                break;
            case SmsManager.RESULT_ERROR_NO_SERVICE:
                Toast.makeText(mainActivity, "No service",
                        Toast.LENGTH_SHORT).show();
                mainActivity.printlnToUser("Error while SMS sending: No service");
                break;
            case SmsManager.RESULT_ERROR_NULL_PDU:
                Toast.makeText(mainActivity, "Null PDU",
                        Toast.LENGTH_SHORT).show();
                mainActivity.printlnToUser("Error while SMS sending: Null PDU");
                break;
            case SmsManager.RESULT_ERROR_RADIO_OFF:
                Toast.makeText(mainActivity, "Radio off",
                        Toast.LENGTH_SHORT).show();
                mainActivity.printlnToUser("Error while SMS sending: Radio off");
                break;

        }
    }
}
